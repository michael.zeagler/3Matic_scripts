import os
from insole_ops import main_directories, project_lister, inter_op 
# temprorarily this outputs to a specified new directory in order to avoid corrupting previous work
# out_top_dir = os.path.join(top_dir,'PROCESSED SCANS (AUTOMATED 3MATIC OUTPUTS)')
maindirs = main_directories()

gen_blks_top_dir = maindirs.gen_blks_top_dir
sld_lib          = maindirs.sld_lib
out_top_dir      = maindirs.threematic_out_top_dir

if os.path.exists(out_top_dir) == False:
    os.mkdir(out_top_dir)

boolean_list = project_lister(gen_blks_top_dir,'3MATIC')

for insole in boolean_list: # looping through projects (customers)
    ##### for final version with proper project indexing from filename convention
    ### checks to see if directory exists, then continues with the boolean if no directory exists
    ### if there is an error, investigating the error can come first
    ### then you can retry after deleting the result or cutting it out of the higher directory

    ### looping through the list of individual insoles for each project

    try:
    	inter_op(insole,out_top_dir,gen_blks_top_dir ,sld_lib)
    	# print(cell_list)
    except FileNotFoundError as e:
    	print('Files needed for operation %s are not present.' % insole)
    	print(e)
    except FileExistsError as e:
    	print(e)