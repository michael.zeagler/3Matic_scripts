"""
"Module" containing the function that pulls the individual files from the insole libraries,
then performs the intersection boolean on the generated infil files, and also contains some useful,
specific utilities for the project.
""" 
import os
from os import walk as walk
from os.path import join as join
from shutil import copy2
from csv import DictReader
from pathlib import Path
from math import sin, cos, pi

class InsoleID: # works
    """Parses the naming convention into relevant entities.\n
    for a png:\n
    [8 character id]_CU[model num][odd/even][gender][size - 3 characters][side letter][weight-a/b/c].png\n 
    example - '00000000_CU00OM130L' """
    
    def __init__(self, file_name_string):

        # In the case of parsing a .png file, different information is needed.
        # Not quite sure about this polymorphism but it's functioning for now.

            name_pieces  = file_name_string.split('_')

            self.cust_id    = name_pieces[0]
            self.solid_id   = name_pieces[1]

            self.model      = self.solid_id[0 :4] 
            self.odd_even   = self.solid_id[4 :5] # odd or even number of lateral cells
            self.gender     = self.solid_id[5 :6]
            self.size       = self.solid_id[6 :9]
            self.side       = self.solid_id[9 :10]
            self.weight     = self.solid_id[10:11]

            self.solid_id   = self.model+self.gender+self.size+self.side


class main_directories: # works-this could probably be a dictionary as most of the output terms are useful 
    """sets the working directories for the scripting operation"""

    def __init__(self):

        self.top_dir                = join('X:%s' %os.sep,'AETREX GEN 1 PRODUCTION ONLY')

        ### dump for the png files from AETREX - input to CustomInsole.exe
        self.png_top_dir            = join(self.top_dir,'FROM THE SCANNER (INCOMING PNG)') 
        ### output of CustomInsole.exe
        self.gen_blks_top_dir       = join(self.top_dir,'INPUT TO 3MATIC SCRIPT')
        ### inputs to boolean_intersection_script.py
        self.sld_lib                = join(self.top_dir,'MODELS')### location of the insole and footbed shapes
        self.threematic_out_top_dir = join(self.top_dir,'OUTPUT FROM 3MATIC SCRIPT')### final destination of boolean-ed stl files
        # self.threematic_out_top_dir = join('C:%s' %os.sep, 'Users','zeamik','Desktop','PROCESSED SCANS (AUTOMATED 3MATIC OUTPUTS)')# test output directory # writing to local directory for testing 

def project_lister(sourcefiles_top_dir,TYPE_OF_OPERATION): 
    """Breaks a list of directory names out of the walk output of the directory where the project's source files are located (.stl or .png for the respective scripts). """ 

    if TYPE_OF_OPERATION == 'SIA':
        # Tentative - need to figure out the different os operations to parse the different source files. If the .png files are within a project specific directory this index will need to be changed to [0][2].
        proj_list = list(walk(sourcefiles_top_dir))[0][1]
        
    elif TYPE_OF_OPERATION =='3MATIC':
        templist = list(walk(sourcefiles_top_dir))[0][1] # list of directories containing the .stl block files
        proj_list = [x for x in templist if x != '_OLD'] # this folder must be excluded

    else:
        print('"project_lister" needs operation type "SIA" or "3MATIC."')
        raise TypeError 

    return proj_list

class ProjectDirs:
    """Identifies the directories specific to the desired operation, as well as the list of .stl files in the project directory"""

    def __init__(self,operation_ID_string, model, gen_blks_top_dir , sld_lib): # will return an empty result until there are actual propertly formatted sources of .stl files to manipulate
    
    

        ### idendifying directory of infil files from scan processing and making a list of filenames
        self.stl_blocks_dir = join(gen_blks_top_dir ,operation_ID_string)
        self.stl_blocks_dir_contents = list(walk(self.stl_blocks_dir))[0][2]
        
        ### declaring the appropriate directories for parts library
        self.footbeds_dir = join(sld_lib,model+' MODELS', model+' SOLID MODELS') # production directory
        # footbeds_dir = join(sld_lib,'Footbed Models') # local test directory
        self.top_surfdir = join(sld_lib,model+' MODELS', model+' TOP SKIN MODELS')
        # top_surfdir = join(sld_lib,'Top Skin Models')
        self.metpad_dir  = join(sld_lib,model+' MODELS', model+' METPAD MODELS')
        # metpad_dir = join(sld_lib,'Metpad Models')

        ### applying a filter to get only the stl files of the blocks for 3matic should there be other directory contents
        self.stl_list = [x for x in self.stl_blocks_dir_contents if x[-4::] == '.stl']

class OpParameters: # 
    """This subclasses several other more limited classes to identify a project and its directories, and makes a method to create the directory at the end of the boolean script."""
    def __init__(self,project_name_string,out_top_dir,gen_blks_top_dir ,sld_lib):

        self.insole = InsoleID(project_name_string)

        self.dirs = ProjectDirs(project_name_string,self.insole.model,gen_blks_top_dir,sld_lib)
        
        # self.output_dir = join(out_top_dir, self.insole.cust_id, 'RTP', self.insole.side)
        ### this version of the output directory applies if using the trimatic save-as-project function
        # self.output_dir = join(out_top_dir, self.insole.cust_id, 'RTP')
        self.output_dir = out_top_dir # for now 

    def make_output_dir(self):
        os.makedirs(self.output_dir)


def inter_op(project,out_top_dir,gen_blks_top_dir ,sld_lib): # 
    """ This function calls 3matic API to perform the boolean intersection and dumps the final result in the appropriate directory. 'project' is the full filename string of an insole within a customer/project directory."""

    ### this import is done within this function to avoid import errors when testing outside of 3matic
    from trimatic.file import import_part_stl, export_stl_binary, clear, new_project, save_project
    from trimatic.design import boolean_intersection
    from trimatic import quick_label, create_point, import_part_stl, clear, view_default,\
     DefaultViews, view_custom, auto_fix
    
    param = OpParameters(project,out_top_dir,gen_blks_top_dir ,sld_lib) # 
    
    stl_list       = param.dirs.stl_list
    output_dir     = param.output_dir
    metpad_dir     = param.dirs.metpad_dir
    top_surfdir    = param.dirs.top_surfdir
    footbeds_dir   = param.dirs.footbeds_dir 
    stl_blocks_dir = param.dirs.stl_blocks_dir
    model          = param.insole.model
    solid_id       = param.insole.solid_id
    cust_id        = param.insole.cust_id
    csvfilepath    = join(main_directories().top_dir,'InsoleLabelData.csv')
    gender         = param.insole.gender
    size           = param.insole.size
    side           = param.insole.side

    # ensuring that files are not overwritten or recalculated without being deleted first
    outputfile = Path(join(output_dir,project+'.mxp'))
    if outputfile.exists():
        raise FileExistsError('The output file for '+project+' already exists.')
        return false
    # outputfile.resolve() # will hopefully throw error 

    ## boolean operation on the list of SIA generated cells 
    new_project()

    topsurf = import_part_stl(join(top_surfdir,solid_id+'_TS.stl'))
    topsurf.name = 'TS_' + cust_id + '_' + solid_id + '_TS'

    if model == '05' or model == '25':
        metpad = import_part_stl(join(metpad_dir,solid_id+'_MP.stl'))
        metpad.name = 'MP_' + cust_id + '_' + solid_id + '_MP'
    # ### the cell_list is a method to generate a list to export as stl files
    # ### when exporting as a project this is not necessary
    

    cell_list = []
    for block in stl_list:
        try: # empty cells will raise an import failed error, so this acts as a filter
                # imports .stl files with the trimatic.Part instance having the same name as the .stl file
                cell_list.append(import_part_stl(join(stl_blocks_dir,block),fix_normals = True))
        except RuntimeError: # RuntimeError potential source of issues?
            print('The file '+block+' failed import. Probable empty cell.')

    cleancell = []
    for x in range(len(cell_list)):

        ### as the intersection mutates (deletes) its inputs, these temporary values are necessary
        tempfootbed = import_part_stl(join(footbeds_dir,solid_id+'.stl'),fix_normals = True) 
        
        init_cell   = boolean_intersection([cell_list[x],tempfootbed])
        auto_fix(init_cell)

        # tempfootbed = import_part_stl(join(footbeds_dir,solid_id+'.stl'),fix_normals = True)
        # tempcell    = boolean_intersection([init_cell,tempfootbed])
        tempcell    = init_cell ###### modification for single boolean pass
        
        ### renaming the results of the boolean operation, which do not keep the names of the original blocks
        tempcell.name = stl_list[x][-9:-6]+stl_list[x][-5:-4]+'_'+stl_list[x][0:-10]+'_'+stl_list[x][-9:-6]+stl_list[x][-5:-4] # prepending the color name for the printer's loading preference

        cleancell.append(tempcell)

    #### applying label to the top skin
    with open(csvfilepath, newline = '') as csvfile:
        reader = DictReader(csvfile)
        csvdict = {x['Size']:[float(x['x']),float(x['y']),float(x['Angle'])] for x in reader}


    input_coords = csvdict[gender+size+side]

    x             = input_coords[0]
    y             = input_coords[1]
    label_degrees = input_coords[2]
    label_radians = pi/180*label_degrees

    inputpoint = create_point([x,y,0])
    directionpoint = create_point([sin(label_radians),cos(label_radians),0])

    # need to figure out proper reference to trimatic.Part within this interface

    quick_label(\
    entity          = topsurf\
    ,text           = cust_id\
    ,point          = inputpoint\
    ,direction      = directionpoint\
    ,follow_surface = True\
    ,font_height    = 6\
    ,label_height   = .3\
    ,font           = 'Lucida Sans Typewriter'\
    ,bold           = True)

    ### making output directory in the case that it does not yet exist 
    if os.path.exists(output_dir) == False:
        param.make_output_dir()

    ### saving as a project instead of outputting individual .stl files
    save_project(join(output_dir,project))

    clear() # not happy about this 'clear' operation but I'm not sure 3matic's mutable state will allow anything else
    ### debugging output
    # return cell_list
