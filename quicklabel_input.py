from trimatic import quick_label, create_point, import_part_stl, clear, view_default, DefaultViews, view_custom
from os.path import join
from math import sin, cos, pi
from csv import DictReader
import os
from insole_ops import main_directories

clear()

cust_id = 'Zi3o1(:H'
gender = 'M'
size = '160'
side = 'L'

topsurf = import_part_stl(join(main_directories().sld_lib,'CU00 MODELS','CU00 TOP SKIN MODELS','CU00'+gender+size+side+'_TS.stl'))

view_default(DefaultViews.Bottom)

csvfilepath    = join(main_directories().top_dir,'InsoleLabelData.csv')

with open(csvfilepath, newline = '') as csvfile:
    reader = DictReader(csvfile)
    csvdict = {x['Size']:[float(x['x']),float(x['y']),float(x['Angle'])] for x in reader}


input_coords = csvdict[gender+size+side]

x             = input_coords[0]
y             = input_coords[1]
theta         = input_coords[2]

label_radians = pi/180*theta

inputpoint = create_point([x,y,0])
directionpoint = create_point([sin(label_radians),cos(label_radians),0])

# need to figure out proper reference to trimatic.Part within this interface

quick_label(\
entity          = topsurf\
,text           = cust_id\
,point          = inputpoint\
,direction      = directionpoint\
,follow_surface = True\
,font_height    = 6\
,label_height   = .3\
,font           = 'Lucida Sans Typewriter'\
,bold           = True)