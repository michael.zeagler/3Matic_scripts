import os
import pyautogui
from pyautogui import hotkey, press, typewrite
from insole_ops import InsoleID, project_lister, project_dirs, main_directories
from time import sleep

png_top_dir = main_directories().png_top_dir
SIA_stl_output_dir = main_directories().SIA_stl_output_dir
gen_blks_top_dir = main_directories().gen_blks_top_dir

pyautogui.pause = 1

### when true, moving the mouse to the upper left cancels the script
# pyautogui.FAILSAFE = True

project_list = project_lister(png_top_dir,'SIA')

print(project_list)

for proj in project_list:
	projdir   = os.path.join(png_top_dir,proj)
	projsides = list(os.walk(projdir))[2]
	for LorR in projsides:
		output_dir = os.path.join(stl_top_dir,side)
		if os.path.exists(output_dir) == False:
			os.mkdir(os.path.join(stl_top_dir,proj))

			filename = LorR # check to make sure this is actually the file name
			insole   = InsoleID(filename)
			model    = insole.model
			side     = insole.side
			odd_even = insole.odd_even
			gender   = insole.gender
			size     = insole.size
			side     = insole.side

			### gui navigation to input dialog
			hotkey('alt','tab')

			press(['alt','f','n'])

			### import pressure map
			press('alt')
			press( ['right','down','down','enter'])
			typewrite(os.path.join(png_top_dir,proj+'.png'))
			press('enter')

			sleep(5)

			### import forefoot section
			press('alt')
			press(['right','down','down','down','down','enter'])
			typewrite(os.path.join(png_top_dir,proj+'.png'))
			press('enter')

			sleep(5)

			### import midfoot section
			press('alt')
			press(['right','down','down','down','down','down','enter'])		
			typewrite(os.path.join(png_top_dir,proj+'.png'))
			press('enter')

			sleep(5)

			### import heel section
			press('alt')
			press(['right','down','down','down','down','down','down','enter'])		
			typewrite(os.path.join(png_top_dir,proj+'.png'))
			press('enter')		


			confirm(text = 'Done moving .png?', buttons = 'OK') 


			press('alt')
			press(['right','down','down','down','down','down','down','down','down','down','enter'])		

			sleep(5)

			proj_output_dir = os.path.join(gen_blks_top_dir,cust_id + '_' + solid_id)
			os.makedirs(proj_output_dir)

			# here would go the save dialogue as the CustomInsole software now has one


pyautogui.alert(text = 'script complete')